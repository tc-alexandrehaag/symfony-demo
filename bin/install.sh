#!/usr/bin/env bash

set -e

PROJECT_PATH=$(dirname $(cd $(dirname $0) && pwd))
CONSOLE="php bin/console"
exec_cmd () {
    echo Exec : $@
    sh -c "$@"
}

exec_cmd "composer install"
exec_cmd "${CONSOLE} doctrine:database:create"
exec_cmd "${CONSOLE} doctrine:schema:update --force"
exec_cmd "${CONSOLE} doctrine:fixtures:load -n"
