<?php

namespace App\Tests;

use App\Entity\Post;
use Codeception\Util\HttpCode;

class BlogCest
{
    public function postsOnIndexPage(FunctionalTester $I)
    {
        $I->amOnPage('/en/blog/');
        $I->seeNumberOfElements('article.post', Post::NUM_ITEMS);
    }

    public function postSeeFirstPost(FunctionalTester $I)
    {
        $I->amOnPage('/en/blog/');
        $I->seeCurrentRouteIs('blog_index');
        $I->click('article.post > h2 a');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeCurrentRouteIs('blog_post');
    }

}
