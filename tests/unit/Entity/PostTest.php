<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Utils;

use App\Entity\Post;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Unit test for the application utils.
 *
 * See https://symfony.com/doc/current/book/testing.html#unit-tests
 *
 * Execute the application tests using this command (requires PHPUnit to be installed):
 *
 *     $ cd your-symfony-project/
 *     $ ./vendor/bin/phpunit
 */
class PostTest extends TestCase
{

    /**
     * @dataProvider getValidAuthorUsername
     * @param $username
     * @param $expected
     */
    public function testGetAuthorUsername($username, $expected)
    {
        $user = $this->createMock(User::class);
        $user->method('getUsername')
            ->willReturn($username);

        $post = new Post();
        $post->setAuthor($user);

        $this->assertSame($expected, $post->getAuthorUsername());
    }

    public function getValidAuthorUsername()
    {
        yield ["test", "test"];
        yield ["123", "123"];
    }
}
