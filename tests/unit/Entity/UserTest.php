<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Utils;

use App\Entity\Post;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Unit test for the application utils.
 *
 * See https://symfony.com/doc/current/book/testing.html#unit-tests
 *
 * Execute the application tests using this command (requires PHPUnit to be installed):
 *
 *     $ cd your-symfony-project/
 *     $ ./vendor/bin/phpunit
 */
class UserTest extends TestCase
{


    /**
     * @dataProvider getValidUserNames
     * @param $username
     * @param $expected
     */
    public function testGetUsername($username, $expected)
    {
        $user = new User();
        $user->setUsername($username);
        $this->assertSame($expected, $user->getUsername());
    }

    public function getValidUserNames()
    {
        yield ['test', 'test'];
        yield ["123", "123"];
        yield ["", ""];
    }

    /**
     * @dataProvider getInvalidUserNames
     * @param $username
     */
    public function testGetUsernameThrowException($username)
    {
        $this->expectException(\TypeError::class);
        $user = new User();
        $user->setUsername($username);
    }

    public function getInvalidUserNames()
    {
        yield [null];
        yield [new User()];
    }
}
