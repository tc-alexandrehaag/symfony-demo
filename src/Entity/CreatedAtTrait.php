<?php

namespace App\Entity;


trait CreatedAtTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        if ($this->createdAt) {
            return $this->createdAt;
        }
        $this->updateCreatedDate();
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateCreatedDate()
    {
        $this->setCreatedAt(new \Datetime());
    }
}
